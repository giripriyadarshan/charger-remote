let device, server, service;
const serviceUuid = '4fafc201-1fb5-459e-8fcc-c5c9c331914b';
const writeUuid = 'beb5483e-36e1-4688-b7f5-ea07361b26a8';
const readUuid = '9fde8759-ffd6-40d7-a50e-f0ffa74abd25';

async function readData() {

    if (!device || !server || !service) {
        try {
            device = await navigator.bluetooth.requestDevice({ filters: [{ services: [serviceUuid] }] });
        } catch (error) {
            console.log(error);
        }
    }

    try {
        server = await device.gatt.connect();
        service = await server.getPrimaryService(serviceUuid);
        let readcharacteristic = await service.getCharacteristics(readUuid);
        let value = decodeValues(await readcharacteristic[0].readValue());
        if (value == '-1') {
            document.getElementById('time-left').innerHTML = 'Charging complete';
            document.getElementById('time-left').style.opacity = "0.5";
        }
        else {
            document.getElementById('time-left').innerHTML = value + ' minutes left';
            document.getElementById('time-left').style.opacity = "1";
        }
    }
    catch (error) {
        console.log(error);
    }

    device.gatt.disconnect();
}

async function writeData() {
    if (!device || !server || !service) {
        try {
            device = await navigator.bluetooth.requestDevice({ filters: [{ services: [serviceUuid] }] });
        } catch (error) {
            console.log(error);
        }
    }

    try {
        server = await device.gatt.connect();
        service = await server.getPrimaryService(serviceUuid);
        let writecharacteristic = await service.getCharacteristics(writeUuid);
        let value = document.getElementById('time-input').value;
        if (value && value != '0') {
            writecharacteristic[0].writeValue(encodeValues(value));
        }
    } catch (error) {
        console.log(error);
    }

    readData();
}

function decodeValues(result) {
    let decoder = new TextDecoder('utf-8');
    return decoder.decode(result.buffer);
}

function encodeValues(stringValue) {
    let encoder = new TextEncoder('utf-8');
    return encoder.encode(stringValue);
}

window.onbeforeunload = closingCode;
function closingCode() {
    if (device) {
        device.gatt.disconnect();
    }
    return null;
}